import base64
import os
import unittest

import numpy as np
import matplotlib.pyplot as plt
import hashlib

from artificial.transferstyle.bot import TransferBot


class TestTransferBot(unittest.TestCase):

    def setUp(self) -> None:
        files = os.listdir()
        self.root = '../resources' if 'test_bot.py' in files else 'test/resources'

    def test_load_image(self):
        img = TransferBot.load_img('%s/load_test.png' % self.root)
        self.assertEqual([1, 601, 501, 3], img.shape)
        red = img[0, :200, :]
        self.assertTrue(np.all(red == [1, 0, 0]))

        green = img[0, 203:400, :]
        self.assertTrue(np.all(green == [0, 1, 0]))

        blue = img[0, 404:, :]
        self.assertTrue(np.all(blue == [0, 0, 1]))

    def test_tensor_to_image(self):
        # Input
        tensor = np.array([[[[0, 0, 1], [0, 0, 0], [1, 0, 0]],
                            [[0, 0, 0], [0, 1, 0], [0, 0, 0]],
                            [[1, 0, 0], [0, 0, 0], [0, 0, 1]]]])

        # Expected
        exp = np.array([[[0, 0, 255], [0, 0, 0], [255, 0, 0]],
                        [[0, 0, 0], [0, 255, 0], [0, 0, 0]],
                        [[255, 0, 0], [0, 0, 0], [0, 0, 255]]])

        # Test & Verify
        res = TransferBot.tensor_to_image(tensor)
        np.testing.assert_equal(exp, res)

    def test_pick_file(self):
        files = [TransferBot.pick_file('%s/pick_test' % self.root) for _ in range(20)]

        # Check random works
        self.assertFalse(all([f == files[0] for f in files]))

        # Check path are good
        paths = ['%s/pick_test/file-%d' % (self.root, i) for i in [1, 2, 3]]
        self.assertTrue(all([f in paths for f in files]))

    def notest_paint(self):
        bot = TransferBot('picasso', self.root)
        img = bot.paint(1000, 1000)

        m = hashlib.sha256()
        m.update(img.tobytes())
        hash = base64.encodebytes(m.digest()).decode('ascii')
        self.assertEqual('g19pYwyIOMcvkJw3Mw0yyDx125/dPEHozvfvj8T261Y=\n', hash)

        #plt.imshow(img)
        #plt.show()
