import unittest

import numpy as np
import matplotlib.pyplot as plt

from artificial.mondrian.bot import Line, Draw, _draw_horizontal_lines, _convert_matrix, paint, _draw_vertical_lines, \
    _split_lines, _prune_lines, _convert_rgb, _fill_color_rec


class TestBot(unittest.TestCase):
    def test_draw_horizontal_lines(self):
        # Input
        draw = Draw(100, 20)

        # Test
        lines = _draw_horizontal_lines(draw)
        for line in lines:
            self.assertEqual(0, line.xa)
            self.assertEqual(draw.w, line.xb)
            self.assertEqual(draw.ticks, line.ya - line.yb)

    def test_draw_vertical_lines(self):
        # Input
        draw = Draw(100, 20)

        # Test
        lines = _draw_vertical_lines(draw)
        for line in lines:
            self.assertEqual(draw.h, line.ya)
            self.assertEqual(0, line.yb)
            self.assertEqual(draw.ticks, line.xb - line.xa)

    def test_convert_space(self):
        # Input
        draw = Draw(10, 10)
        draw.lines = [Line(0, 10, 7, 0), Line(7, 10, 10, 3)]

        # Expected
        exp = np.array([[1, 1, 1, 1, 1, 1, 1, 0, 0, 0],
                        [1, 1, 1, 1, 1, 1, 1, 0, 0, 0],
                        [1, 1, 1, 1, 1, 1, 1, 0, 0, 0],
                        [1, 1, 1, 1, 1, 1, 1, 1, 1, 1],
                        [1, 1, 1, 1, 1, 1, 1, 1, 1, 1],
                        [1, 1, 1, 1, 1, 1, 1, 1, 1, 1],
                        [1, 1, 1, 1, 1, 1, 1, 1, 1, 1],
                        [1, 1, 1, 1, 1, 1, 1, 1, 1, 1],
                        [1, 1, 1, 1, 1, 1, 1, 1, 1, 1],
                        [1, 1, 1, 1, 1, 1, 1, 1, 1, 1]])

        res = _convert_matrix(draw)
        np.testing.assert_equal(exp, res)

    def test_convert_rgb(self):
        # Input
        matrix = np.array([[0, 1],
                           [1, 0]])

        # Expected
        exp = np.array([[[255, 255, 255], [0, 0, 0]],
                        [[0, 0, 0], [255, 255, 255]]])

        # Test & Verify
        res = _convert_rgb(matrix)
        np.testing.assert_equal(exp, res)

    def test_fill_color_rec(self):
        # Input
        matrix = np.array([[1, 1, 1, 1],
                           [1, 0, 0, 0],
                           [1, 1, 0, 0],
                           [1, 0, 0, 0],
                           [1, 1, 1, 1],
                           [0, 0, 0, 0]])

        # Expected
        exp = np.array([[1, 1, 1, 1],
                        [1, 3, 3, 3],
                        [1, 1, 3, 3],
                        [1, 3, 3, 3],
                        [1, 1, 1, 1],
                        [0, 0, 0, 0]])

        # Test & Verify
        res, _ = _fill_color_rec(matrix, 1, 2, 3)
        np.testing.assert_equal(exp, res)

    def test_split_lines(self):
        # Input
        draw = Draw(10, 10)
        hlines = [Line(0, 7, 10, 3)]
        vlines = [Line(2, 10, 4, 0), Line(6, 10, 8, 0)]

        # Expected
        expLines = [Line(0, 7, 4, 3), Line(2, 7, 8, 3), Line(6, 7, 10, 3),  # Vertical
                    Line(2, 10, 4, 3), Line(2, 7, 4, 0), Line(6, 10, 8, 3), Line(6, 7, 8, 0)]

        # Test & Verify
        draw = _split_lines(hlines, vlines, draw)
        self.assertEqual(expLines, draw.lines)

    def test_prune_lines(self):
        draw = Draw(10, 10)
        draw.lines = [Line(0, 10, 10, 0)] * 100

        res = _prune_lines(draw)
        self.assertEqual(70, len(res.lines))

    def test_paint(self):
        matrix = paint(75, 50)
        plt.imshow(matrix)
        # plt.show()


class TestLine(unittest.TestCase):
    def test_is_split(self):
        a = Line(10, 20, 50, 10)

        # Test horizontal split
        self.assertTrue(a.is_split(Line(10, 30, 20, 10)))
        self.assertFalse(a.is_split(Line(30, 40, 50, 30)))
        self.assertFalse(a.is_split(Line(30, 8, 50, 0)))

        # Test vertical split
        self.assertTrue(a.is_split(Line(0, 15, 50, 13)))
        self.assertFalse(a.is_split(Line(0, 15, 8, 13)))
        self.assertFalse(a.is_split(Line(52, 15, 56, 13)))

