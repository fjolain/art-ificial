from typing import List, Tuple
from PIL import Image
import numpy as np

__all__ = ['paint']

from numpy.random.mtrand import randint


class Line:
    """
    Dark Line used in painting
    """

    def __init__(self, xa, ya, xb, yb):
        """
        Instance box.

        :param xa: x coord of upper left corner
        :param ya: y coord of upper left corner
        :param xb: x coord of bottom right corner
        :param yb: y coord of bottom right corner
        """
        self.xa = xa
        self.ya = ya
        self.xb = xb
        self.yb = yb

    def is_split(self, line) -> bool:
        """
        Does line split current line ?

        :param line: other line to test
        :return: True if other line split current line
        """
        return (line.ya >= self.ya and line.yb <= self.yb) or (line.xa <= self.xa and line.xb >= self.xb)

    def __eq__(self, o):
        if not isinstance(o, Line):
            return False
        return self.xa == o.xa and self.ya == o.ya and self.xb == o.xb and self.yb == o.yb

    def __str__(self):
        return '(%d, %d, %d, %d)' % (self.xa, self.ya, self.xb, self.yb)

    def __repr__(self):
        return '(%d, %d, %d, %d)' % (self.xa, self.ya, self.xb, self.yb)


class Draw:
    """
    Regroup useful values to draw
    """
    def __init__(self, w: int, h: int):
        """
        Instance

        :param w: painting width
        :param h: painting height
        """
        self.ticks = int(max(w, h) * 0.02)
        self.w = w
        self.h = h
        self.lines = []


def _draw_horizontal_lines(draw: Draw) -> List[Line]:
    """
    Draw horizontal lines randomly separated.

    :param draw: current draw config
    :return: list of new lines generated
    """
    ypos = randint(draw.h/10, draw.h/5)
    lines = []
    while ypos < draw.h:
        lines.append(Line(0, ypos+draw.ticks, draw.w, ypos))
        ypos += randint(draw.h / 10, draw.h / 5)
    return lines


def _draw_vertical_lines(draw: Draw) -> List[Line]:
    """
    Draw vertical lines randomly separated.

    :param draw: current draw config
    :return: list of new lines generated
    """
    xpos = randint(draw.w/10, draw.w/5)
    lines = []
    while xpos < draw.w:
        lines.append(Line(xpos, draw.h, xpos+draw.ticks, 0))
        xpos += randint(draw.w / 10, draw.w / 5)
    return lines


def _convert_matrix(draw: Draw) -> np.ndarray:
    """
    Convert a draw config with all lines into 2D matrix. Line pixel will be fixed at 1 value, blank at 0.

    :param draw: current draw config
    :return: 2D matrix with binary value (1 = line, 0 blank)
    """
    matrix = np.zeros((draw.h, draw.w), dtype='uint8')
    for line in draw.lines:
        matrix[line.yb:line.ya, line.xa:line.xb] = 1

    return matrix


def _convert_rgb(matrix: np.ndarray) -> np.ndarray:
    """
    Convert 2D matrix to 3D matrix with RGB color.
    * 0 -> white    [255, 255, 255]
    * 1 -> black    [  0,   0,   0]
    * 2 -> blue     [ 33,  36, 232]
    * 3 -> red      [235,  57,  41]
    * 4 -> yellow   [250, 226,  79]

    :param matrix: 2D matrix with range in [0, 1, 2, 3, 4]
    :return: 3D matrix with values converted in RGB color
    """
    def _swap(value, rgb_values, arr):
        i = (arr == value).nonzero()
        np.put(arr, i, rgb_values)

    rgb = np.repeat(matrix, 3)

    _swap(0, [255, 255, 255], rgb) # Swap white
    _swap(1, [  0,   0,   0], rgb) # Swap black
    _swap(2, [ 33,  36, 232], rgb) # Swap blue
    _swap(3, [235,  57,  41], rgb) # Swap red
    _swap(4, [250, 226,  79], rgb) # swap yellow

    x, y = matrix.shape
    return rgb.reshape(x, y, 3)


def _fill_color_rec(matrix: np.ndarray, x, y, color: int, path: List = None) -> Tuple[np.ndarray, List]:
    """
    Fill all blank zone pointed by (x, y) coordinate with color values.

    :param matrix: matrix to fill
    :param x: x coordinate to fill
    :param y: y coordinate to fill
    :param color: color value inside [2, 3, 4]
    :param path: used by recursion DON'T PROVIDE IT !
    :return: (matrix full by color, path)
    """
    path = list() if path is None else path

    # Stop here if out of size, line pixel or already in path
    if x >= matrix.shape[0] or y >= matrix.shape[1] or matrix[x, y] > 0 or (x, y) in path:
        return matrix, path
    # Add pixel in path, apply color and look at neighbors
    else:
        matrix[x, y] = color
        path.append((x, y))

        matrix, path = _fill_color_rec(matrix, x + 1, y    , color, path)
        matrix, path = _fill_color_rec(matrix, x - 1, y    , color, path)
        matrix, path = _fill_color_rec(matrix, x    , y + 1, color, path)
        matrix, path = _fill_color_rec(matrix, x    , y - 1, color, path)
        return matrix, path


def _split_lines(hlines: List[Line], vlines: List[Line], draw: Draw) -> Draw:
    """
    Examine line, if some line override other, lines are split.

    :param hlines: horizontal lines to examine
    :param vlines: vertical lines to examine
    :param draw: current draw config
    :return: new draw with all split line inside.
    """
    draw.lines = []

    # Spit horizontal lines
    for h in hlines:
        for v in vlines:
            if h.is_split(v):
                draw.lines.append(Line(h.xa, h.ya, v.xb, h.yb))
                h = Line(v.xa, h.ya, h.xb, h.yb)
        draw.lines.append(h)

    # Split vertical lines
    for v in vlines:
        for h in hlines:
            if v.is_split(h):
                draw.lines.append(Line(v.xa, v.ya, v.xb, h.yb))
                v = Line(v.xa, h.ya, v.xb, v.yb)
        draw.lines.append(v)

    return draw


def _prune_lines(draw: Draw) -> Draw:
    """
    Randomly prune 30% of lines.

    :param draw: current draw
    :return: new draw with 30% of lines deleted
    """
    # Prune 30 %
    size = int(len(draw.lines) * 0.3)

    for _ in range(size):
        i = randint(len(draw.lines))
        del draw.lines[i]

    return draw


def paint(w: int, h: int) -> np.ndarray:
    """
    Main function to paint like Mandrian.

    :param w: width painting
    :param h: height painting
    :return: 3D matrix with rgb values
    """
    draw = Draw(75, 50)
    hlines = _draw_horizontal_lines(draw)
    vlines = _draw_vertical_lines(draw)

    draw = _split_lines(hlines, vlines, draw)
    draw = _prune_lines(draw)

    matrix = _convert_matrix(draw)

    for color in [2, 3, 4] * 3:
        x, y = randint(matrix.shape[0]), randint(matrix.shape[1])
        matrix, _ = _fill_color_rec(matrix, x, y, color)

    matrix = _convert_rgb(matrix)
    matrix = np.array(Image.fromarray(matrix).resize(size=(w, h), resample=Image.NEAREST))
    return matrix
