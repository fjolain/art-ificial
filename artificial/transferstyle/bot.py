import os

import tensorflow as tf
import tensorflow_hub as hub

import numpy as np


class TransferBot:
    def __init__(self, style: str, root: str = '.'):
        self.style_dir = '%s/%s' % (root, style)
        self.content_dir = '%s/content' % root
        self.hub_module = hub.load('https://tfhub.dev/google/magenta/arbitrary-image-stylization-v1-256/1')

    def paint(self, w: int, h: int) -> np.ndarray:
        content_image = TransferBot.load_img(TransferBot.pick_file(self.content_dir), max_dim=500)
        style_image = TransferBot.load_img(TransferBot.pick_file(self.style_dir), max_dim=500)
        stylized_image = self.hub_module(tf.constant(content_image), tf.constant(style_image))[0]
        return TransferBot.tensor_to_image(stylized_image)

    @staticmethod
    def pick_file(dir: str):
        images = os.listdir(dir)
        i = np.random.randint(0, len(images))
        image = images[i]
        return '%s/%s' % (dir, image)

    @staticmethod
    def load_img(path_to_img, max_dim=None):
        img = tf.io.read_file(path_to_img)
        img = tf.image.decode_image(img, channels=3)
        img = tf.image.convert_image_dtype(img, tf.float32)

        if max_dim is not None:
            shape = tf.cast(tf.shape(img)[:-1], tf.float32)
            long_dim = max(shape)
            scale = max_dim / long_dim

            new_shape = tf.cast(shape * scale, tf.int32)
            img = tf.image.resize(img, new_shape)

        img = img[tf.newaxis, :]
        return img

    @staticmethod
    def tensor_to_image(tensor):
        tensor = np.array(tensor * 255, dtype=np.uint8)
        if np.ndim(tensor) > 3:
          assert tensor.shape[0] == 1
          tensor = tensor[0]

        return tensor