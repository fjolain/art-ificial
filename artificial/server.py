import os
from io import BytesIO

import matplotlib.pyplot as plt
from flask import Flask, render_template, send_file, request
from flask_cors import cross_origin

from artificial.mondrian.bot import paint as md_paint
from artificial.transferstyle.bot import TransferBot

application = Flask(__name__)

origin = os.getenv('ALLOW_ORIGIN', 'localhost')
root = os.getenv('DATA_ROOT', '../data')

vangogh = TransferBot(style='vangoth', root=root)
monet = TransferBot(style='monet', root=root)
picasso = TransferBot(style='picasso', root=root)


@application.route('/')
@cross_origin(origin=origin, headers=['Content-Type', 'Authorization'])
def home():
    host = request.host
    return render_template('index.html', host=host)


@application.route("/favicon.ico")
@cross_origin(origin=origin, headers=['Content-Type', 'Authorization'])
def favicon():
    return send_file('favicon.ico', 'image/vnd.microsoft.icon')


@application.route("/api/v1/default")
@cross_origin(origin=origin, headers=['Content-Type', 'Authorization'])
def default_paint():
    return send_file('default.png', 'image/png')


@application.route("/api/v1/paint/<name>")
@cross_origin(origin=origin, headers=['Content-Type', 'Authorization'])
def paint(name: str = 'Mandrian') -> dict:
    w = request.args.get('w', type=int)
    h = request.args.get('h', type=int)

    if name == 'Van Gogh':
        matrix = vangogh.paint(w, h)
    elif name == 'Monet':
        matrix = monet.paint(w, h)
    elif name == 'Picasso':
        matrix = picasso.paint(w, h)
    else:
        matrix = md_paint(w, h)

    output = BytesIO()
    plt.imsave(output, matrix, format='png')
    output.seek(0)
    return send_file(output, 'image/png')


if __name__ == '__main__':
    application.run(debug=True, host='0.0.0.0')